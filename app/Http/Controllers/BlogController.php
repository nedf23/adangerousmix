<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::where('status', 'published')
          ->orderBy('created_at', 'desc')
          ->paginate(10);

        return view('blog.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();

        return view('blog.add', compact('categories'));
    }

    public function store(Request $request)
    {
        $post = Post::create($request->all());

        $this->savePostTags($post, $request->get('tags'));

        return redirect('/blog/admin');
    }

    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::all();

        return view('blog.edit', compact('post', 'categories'));
    }

    public function update($id, Request $request)
    {
        $post = Post::find($id);
        $post->title = $request->get('title');
        $post->slug = $request->get('slug');
        $post->content = $request->get('content');
        $post->image_url = $request->get('image_url');
        $post->category_id = $request->get('category_id');
        $post->status = $request->get('status');
        $post->save();

        $this->savePostTags($post, $request->get('tags'));

        return redirect('/blog/admin');
    }

    public function savePostTags($post, $tags)
    {
        $tags = explode(',', $tags);
        $tagArray = [];

        foreach ($tags as $tag) {
            $currentTag = Tag::where('name', trim($tag))->first();

            if (count($currentTag) < 1) {
                $currentTag = Tag::create(['name' => trim($tag)]);
            }

            $tagArray[] = $currentTag->id;
        }

        $post->tags()->sync($tagArray);
    }

    public function destroy($id)
    {
        Post::find($id)->delete();

        return redirect('blog/admin');
    }

    public function getPostBySlug($slug)
    {
        $post = Post::with(['category','tags'])
            ->where('slug', $slug)
            ->first();

        return view('blog.post', compact('post'));
    }

    public function getPostsByCategory($name)
    {
        $category = Category::where('name', $name)->first();
        $posts = Post::with('category')
            ->where('category_id', $category->id)
            ->where('status', 'published')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('blog.category', compact('posts','category'));
    }

    public function getPostsByTag($name)
    {
        $tag = Tag::where('name', $name)->first();

        $posts = Post::with('category')
            ->where('status', 'published')
            ->whereHas('tags', function($q) use ($tag) {
                $q->where('tag_id', $tag->id);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('blog.tag', compact('posts', 'tag'));
    }

    public function admin()
    {
        $posts = Post::orderBy('created_at','desc')->get();

        return view('blog.admin.index', compact('posts'));
    }
}
