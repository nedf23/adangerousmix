<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        return redirect('/blog');
    }

    public function about()
    {
        return view('pages.about');
    }
}
