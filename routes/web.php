<?php

Route::get('/', 'BlogController@index');
Route::get('home', 'PageController@home');
Route::get('about', 'PageController@about');

Route::get('category/{name}', 'BlogController@getPostsByCategory');
Route::get('tag/{name}', 'BlogController@getPostsByTag');

Route::group(['middleware' => 'auth'], function () {
    Route::get('blog/admin', 'BlogController@admin');
    Route::get('blog/posts/add', 'BlogController@create');
    Route::post('blog/posts', 'BlogController@store');
    Route::get('blog/posts/{id}/edit', 'BlogController@edit');
    Route::post('blog/posts/{id}/update', 'BlogController@update');
    Route::get('blog/posts/{id}/delete', 'BlogController@destroy');
});

Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@getPostBySlug');

Auth::routes();
