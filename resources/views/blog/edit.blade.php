@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
    <div class="row">
        <form action="/blog/posts/{{ $post->id }}/update" method="post" class="col-md-6 col-md-offset-3">
            {{ csrf_field() }}

            <h1>Edit Post</h1>

            <div class="form-group">
                <label for="title">Title</label>
                <input name="title" type="text" class="form-control" value="{{ $post->title }}" />
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input name="slug" type="text" class="form-control" value="{{ $post->slug }}" />
            </div>

            <div class="form-group">
                <label for="content">Content (Markdown + HTML)</label>
                <textarea name="content" rows="10" class="form-control">{{ $post->content }}</textarea>
            </div>

            <div class="form-group">
                <label for="image_url">Image URL</label>
                <input name="image_url" type="text" class="form-control" value="{{ $post->image_url }}" />
            </div>

            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" class="form-control">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ ($category->id == $post->category_id) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="tags">Tags (Comma Separated)</label>
                <input name="tags" type="text" class="form-control" value="{{ $post->tags->implode('name', ', ') }}" />
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control">
                    <option value="published" {{ ($post->status == 'published') ? 'selected' : '' }}>PUBLISHED</option>
                    <option value="draft" {{ ($post->status == 'draft') ? 'selected' : '' }}>DRAFT</option>
                    <option value="archived" {{ ($post->status == 'archived') ? 'selected' : '' }}>ARCHIVED</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="/blog/admin">Cancel</a>
            </div>
        </form>
    </div>
@stop
