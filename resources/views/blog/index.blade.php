@extends('layouts.site')

@section('title', 'Blog')

@section('content')
    @if(count($posts) >= 1)
        <div class="list">
            @foreach ($posts as $post)
                <article class="promo">
                    <a href="/blog/{{ $post->slug }}">
                        <img src="{{ $post->image_url }}" alt="{{ $post->title }}">
                        <h1>{{ $post->title }}</h1>
                        <p class="article-meta">
                            <a href="/category/{{ strtolower($post->category->name) }}">{{ $post->category->name }}</a>
                            &nbsp;/&nbsp;
                            {{ $post->created_at->formatLocalized('%B %d, %Y') }}
                        </p>
                    </a>
                </article>
            @endforeach
            @if (count($posts) % 2 != 0)
                <article></article>
            @endif
        </div>
        {{ $posts->links() }}
    @else
        <article>
            <p>No posts found.</p>
        </article>
    @endif
@stop
