@extends('layouts.app')

@section('title', 'Add Post')

@section('content')
    <div class="row">
        <form action="/blog/posts" method="post" class="col-md-6 col-md-offset-3">
            {{ csrf_field() }}

            <h1>Add Post</h1>

            <div class="form-group">
                <label for="title">Title</label>
                <input name="title" type="text" class="form-control" />
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input name="slug" type="text" class="form-control" />
            </div>

            <div class="form-group">
                <label for="content">Content (Markdown + HTML)</label>
                <textarea name="content" rows="10" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <label for="image_url">Image URL</label>
                <input name="image_url" type="text" class="form-control" />
            </div>

            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" class="form-control">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="tags">Tags (Comma Separated)</label>
                <input name="tags" type="text" class="form-control" />
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control">
                    <option value="published">PUBLISHED</option>
                    <option value="draft">DRAFT</option>
                    <option value="archived">ARCHIVED</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add</button>
                <a href="/blog/admin">Cancel</a>
            </div>
        </form>
    </div>
@stop
