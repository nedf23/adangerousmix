@extends('layouts.app')

@section('title', 'Admin')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Posts</h1>
            <div class="clearfix">
                <a href="/" class="btn btn-default pull-right" style="margin: 0 0 0 1rem;">View Site</a>
                <a href="/blog/posts/add" class="btn btn-success pull-right">Add Post</a>
            </div>
            <br />
            @if(count($posts) >= 1)
                <ul class="list-group">
                    @foreach ($posts as $post)
                        <li class="list-group-item">
                            <a href="/blog/{{ $post->slug }}">
                                {{ $post->title }}
                            </a>
                            <span class="pull-right">
                                {{ strtoupper($post->status) }}
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="/blog/posts/{{ $post->id }}/edit">Edit</a>
                                &nbsp;|&nbsp;
                                <a href="/blog/posts/{{ $post->id }}/delete">Delete</a>
                            </span>
                        </li>
                    @endforeach
                </ul>
            @else
                <ul class="list-group">
                    <li class="list-group-item">No posts found.</li>
                </ul>
            @endif
        </div>
    </div>

@stop
