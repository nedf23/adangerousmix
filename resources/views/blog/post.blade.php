@extends('layouts.site')

@section('title')
    {{ $post->title }}
@stop

@section('content')
    <article>
        <img src="{{ $post->image_url }}" alt="{{ $post->title }} Image">

        <p class="article-meta">
            <a href="/category/{{ strtolower($post->category->name) }}">{{ $post->category->name }}</a>
            &nbsp;/&nbsp;
            {{ $post->created_at->formatLocalized('%B %d, %Y') }}
        </p>

        <h1>{{ $post->title }}</h1>

        {!! Markdown::convertToHtml($post->content) !!}

        @if (count($post->tags) > 0)
            <div>
                @foreach ($post->tags as $tag)
                    <a href="/tag/{{ $tag->name }}" class="tag">{{ $tag->name }}</a>&nbsp;
                @endforeach
            </div>
        @endif
        <div class='shareaholic-canvas' data-app='share_buttons' data-app-id='26202389' style="margin-left: -14px; margin-top: 1rem;"></div>
    </article>
@stop

@section('scripts')
    <script type='text/javascript' data-cfasync='false' src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js' data-shr-siteid='256b81c6a74d10f54d1764497515f48d' async='async'></script>
    <script type="text/javascript">
        var spoilerBtn = document.getElementById('spoiler-button');
        spoilerBtn.onclick = function () {
            showSpoiler();
        };

        function showSpoiler() {
            spoilers = document.getElementById('spoilers');

            spoilers.style.display = 'block';
            spoilerBtn.style.display = 'none';
        }
    </script>
@stop
