<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }} - @yield('title')</title>
    @yield('styles')
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <header>
        <h1>
            <a href="/">
                <span class="icon-adm" title="A Dangerous Mix Logo"></span>
                A Dangerous Mix
            </a>
        </h1>
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                @foreach (App\Category::all() as $category)
                    <li><a href="/category/{{ strtolower($category->name) }}">{{ $category->name }}</a></li>
                @endforeach
                @if (auth::check())
                    <li><a href="/blog/admin">Admin</a></li>
                    <li><a href="/logout">Logout</a></li>
                @endif
            </ul>
        </nav>
    </header>
    <section class="main">
        @yield('content')
    </section>
    <footer>
        <p>© {{ date('Y') }} <a href="//nedf23.com">Ned Fenstermacher</a>. All rights reserved.</p>
    </footer>
    @yield('scripts')
    @if (Auth::guest() && Request::root() != 'http://adm.app')
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-84730016-1', 'auto');
            ga('send', 'pageview');
        </script>
    @endif
</body>
</html>
