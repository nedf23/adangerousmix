@extends('layouts.site')

@section('title', 'About')

@section('content')
    <article>
        <h1>About</h1>

        <p>I'm not a critic. I'm just a fan that likes to talk about things I find entertaining.</p>
	<p>Places to find me:</p>
	<ul>
		<li><a href="https://twitter.com/adangerousmix">Twitter</a></li>
		<li><a href="https://facebook.com/adangerousmix">Facebook</a></li>
		<li><a href="https://twitter.com/adangerousmix">Twitch</a></li>
		<li><a href="https://www.instagram.com/adangerousmix/">Instagram</a></li>
    </article>
@stop
