<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Movies'
        ]);

        DB::table('categories')->insert([
            'name' => 'TV'
        ]);

        DB::table('categories')->insert([
            'name' => 'Comics'
        ]);

        DB::table('categories')->insert([
            'name' => 'Games'
        ]);
    }
}
